<?php
$postdata =$_POST;

?>
@extends('layouts.front.app')

@section('content')
<style>
	.success-top{ text-align: center;margin: 50px 50px; }
	.success-top h1 { text-align: center; color: #37BC9B; font-weight: bold;font-size:  72px;}
	.success-top p { text-align: center; font-weight: bold;font-size:  18px;}
	.success-bottom { border: 1px solid #ddd; background-color: #efe; margin-bottom: 50px }
	.success-bottom .table { margin: 20px 0px; border: none; }
	#table-cont { width: 90% }
	.table>tbody>tr>td { padding: 20px 10px; }
	.table tr td{ padding: 20px 10px; }
	.success-bottom h4 { margin-left: 50px; text-align: center; font-weight: bold; color: #aaa; }
	
</style>

<div class="container">
	<div class="row">	
	@if($orderLast->payment == 'cod')
			<div class="success-top">
			<h1>Thank You</h1>
			<p><strong>Order Booked Successfully</strong></p>
			<h6><strong>Order Id : </strong>{{$orderLast->id}}</h6>
		</div>
	@else	
		<div class="success-top">
			<h1>Thank You</h1>
			<p><strong>Payment Received</strong></p>
			<h6><strong>Transaction Id : </strong>{{$postdata['txnid']}}</h6>
		</div>
		@endif
		<div class="success-bottom "><br>
			<h4>
			Order Detail</h4><br>
			<div class="container" id="table-cont">
				<table class="table table-striped">
					<thead>
						<tr>
							<th colspan='2'>Product</th>
							<th>Size</th>
							<th>Quantity</th>
							<th>Total Price</th>
							

						</tr>
					</thead>

					<tbody>
						@foreach($products as $product)

						
						<tr>
							<td>	
								<img src="{{ asset('storage/'.$product->cover ?? '') }}" style="height: 100px; width: auto;">
							</td>
							<td>{{$product->name ?? ''}}
								<hr>
								<?php echo substr($product->description ?? '',0,100) ?>...</td>
							<td>{{ $product->product_size ?? ''}}</td>
							<td>{{ $product->cart_quantity ?? ''}}</td>
							<td>RS. {{ $product->total_price}}</td>
							
						</tr>
						@endforeach
					</tbody>
				</table>		
			</div>	
		</div>
	</div>
</div>

@endsection

