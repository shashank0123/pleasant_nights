@extends('layouts.front.app')

@section('content')
    <div class="container product-in-cart-list">
        @if(count($products)>0)
            <div class="row">
                 
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}"> <i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Shopping Cart</li>
                    </ol>
                </div>
                <div class="col-md-12 content">
                    <div class="box-body">
                        @include('layouts.errors-and-messages')
                    </div>
                    @if(count($addresses) > 0)
                        <div class="row">
                            <div class="col-md-12">
                                @include('front.products.product-list-table', compact('products'))
                            </div>
                        </div>
                        @if(isset($addresses))
                            <div class="row">
                                <div class="col-md-12">
                                    <legend><i class="fa fa-home"></i> Addresses</legend>
                                    <table class="table table-striped">
                                        <thead>
                                            <th>Alias</th>
                                            <th>Address</th>
                                            <th>Billing Address</th>
                                            <th>Delivery Address</th>
                                        </thead>
                                        <tbody>
                                            @foreach($addresses as $key => $address)
                                                <tr>
                                                    <td>{{ $address->alias }}</td>
                                                    <td>
                                                        {{ $address->address_1 }} {{ $address->address_2 }} <br />
                                                        @if(!is_null($address->province))
                                                            {{ $address->city }} {{ $address->province->name }} <br />
                                                        @endif
                                                        {{ $address->city }} {{ $address->state_code }} <br>
                                                        {{ $address->country->name }} {{ $address->zip }}
                                                    </td>
                                                    <td>
                                                        <label class="col-md-6 col-md-offset-3">
                                                        <input
                                                                    type="radio" onclick="getBillAdd({{$address->id}})"
                                                                    value="{{ $address->id }}"
                                                                    name="billing_address"
                                                                    @if(!empty($billingAddress) && $billingAddress->id == $address->id) {{"checked"}}  @endif >
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <!-- @if(!empty($billingAddress) && $billingAddress->id == $address->id)
                                                            <label for="sameDeliveryAddress">
                                                                <input type="checkbox" id="sameDeliveryAddress" checked="checked"> Same as billing
                                                            </label>
                                                        @endif -->


                                                            <label for="sameDeliveryAddress">
                                                                <input type="radio" onclick="getDelAdd({{$address->id}})" id="sameDeliveryAddress" value="{{ $address->id }}" name="delivery_address" @if(!empty($billingAddress) && $billingAddress->id == $address->id) {{'checked'}}  @endif > 
                                                            </label>
                                                        
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tbody style="display: none" id="sameDeliveryAddressRow">
                                            @foreach($addresses as $key => $address)
                                                <tr>
                                                    <td>{{ $address->alias }}</td>
                                                    <td>
                                                        {{ $address->address_1 }} {{ $address->address_2 }} <br />
                                                        @if(!is_null($address->province))
                                                            {{ $address->city }} {{ $address->province->name }} <br />
                                                        @endif
                                                        {{ $address->city }} {{ $address->state_code }} <br>
                                                        {{ $address->country->name }} {{ $address->zip }}
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <label class="col-md-6 col-md-offset-3">
                                                            <input
                                                                    type="radio"
                                                                     onclick="getDelAdd({{$address->id}})"
                                                                    value="{{ $address->id }}"
                                                                    name="delivery_address"
                                                                    @if(old('') == $address->id) {{'checked'}}  @endif>
                                                        </label>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <a href="{{ route('customer.address.create', auth()->user()->id) }}" class="btn btn-primary right">Add New address</a>
                                </div>
                            </div>
                        @endif
                        @if(!empty($rates))
                            <div class="row">
                                <div class="col-md-12">
                                    <legend><i class="fa fa-truck"></i> Courier</legend>
                                    <ul class="list-unstyled">
                                        @if(!empty($rates))
                                        @foreach($rates as $rate)
                                            <li class="col-md-4">
                                                <label class="radio">
                                                    <input type="radio" name="rate" data-fee="{{ $rate->amount }}" value="{{ $rate->object_id }}">
                                                </label>
                                                <img src="{{ $rate->provider_image_75 }}" alt="courier" class="img-thumbnail" /> {{ $rate->currency }} {{ $rate->amount }}<br />
                                                {{ $rate->servicelevel->name }}
                                            </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div> <br>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <legend><i class="fa fa-credit-card"></i> Payment</legend>
                                @if(isset($payments) && !empty($payments))
                                    <table class="table table-striped">
                                        <thead>
                                        <th class="col-md-4">Name</th>
                                        <th class="col-md-4">Description</th>
                                        <th class="col-md-4 text-right">Choose payment</th>
                                        </thead>
                                        <tbody>
                                        @foreach($payments as $payment)
                                        @if(!empty($payment['name']))
                                            <tr>
                                                <td>{{ ucfirst($payment['name'] ?? '') }}</td>
                                                <td>{{ $payment['description'] ?? '' }}</td>
                                                <td><input type="radio" id="p_method" value="{{$payment['name'] ?? ''}}" name="p_method" onclick="getMethod('{{$payment['name'] ?? ''}}')" @if($payment['name']=='cod'){{'checked'}}@endif></td>
                                            </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <p class="alert alert-danger">No payment method set</p>
                                @endif
                            </div>
                        </div>
                    @else
                        <p class="alert alert-danger"><a href="{{ route('customer.address.create', [$customer->id]) }}">No address found. You need to create an address first here.</a></p>
                    @endif

                    <!-- <div class="row right"> -->
                       <form method="post" action="{{ url('postCheckout') }}">
                    @csrf
                    <input type="hidden" name="billing_add" id="billing_add" value="@if(!empty($billingAddress)){{$billingAddress->id}}@endif">
                    <input type="hidden" name="delivery_add" id="delivery_add" value="@if(!empty($billingAddress)){{$billingAddress->id}}@endif">
                            <input type="hidden" name="products" value="{{$product}}">
                            <input type="hidden" name="customer_id" value="{{$customer->id}}">
                            <input type="hidden" name="total" value="{{$totalBill}}">
                            <input type="hidden" name="payment_method" id="payment_method" value="cod">
                            <button type="submit" id="add-to-cart"  name='payment' value="payment" class="btn btn-primary right">Proceed To Payment</button>
                            <br><br>
                        </form>
                    <!-- </div> -->

                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <p class="alert alert-warning">No products in cart yet. <a href="{{ route('home') }}">Show now!</a></p>
                </div>
            </div>
        @endif
    </div>
@endsection