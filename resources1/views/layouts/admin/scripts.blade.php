<script type="text/javascript">
	var count_form = 0;
	function getSizeDiv(){
		var CSRF_TOKEN = $('meta[name="_t"]').attr('content');
    $.ajax({
      type: "post",
      url: "/admin/getMoreFields",
      crossDomain: true,
      data:{ _token: CSRF_TOKEN, show:'page',id: count_form },       
      dataType: 'html',
      success:function(data){
        // alert('yes')
        $('#dynamic-size').append(data);
        count_form++;

      },
      failure:function(data){
       // alert('no')
       $('#dynamic-size').append(data);
     }
   });
	}

	function removeDiv(id){
		$('#sizes_div'+id).hide();
	}

  function deleteSize(id){
    window.location.href = '/admin/delete_size/'+id;
  }
</script>