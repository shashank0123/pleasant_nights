<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * Admin routes
 */
Route::namespace('Admin')->group(function () {
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'LoginController@login')->name('admin.login');
    Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');
});

Route::group(['prefix' => 'admin', 'middleware' => ['employee'], 'as' => 'admin.' ], function () {
    Route::namespace('Admin')->group(function () {



        Route::group(['middleware' => ['role:admin|superadmin|clerk, guard:employee']], function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');
            Route::namespace('Products')->group(function () {

  Route::post('getMoreFields','ProductController@getMoreFields');
  Route::get('delete_size/{id}','ProductController@deleteSize');

                Route::resource('products', 'ProductController');
                Route::get('remove-image-product', 'ProductController@removeImage')->name('product.remove.image');
                Route::get('remove-image-thumb/{src}', 'ProductController@removeThumbnail')->name('product.remove.thumb');
            });
            //Blogs
            Route::namespace('Blog')->group(function () {
                Route::resource('blogs', 'BlogController');
                Route::get('remove-image-product', 'BlogController@removeImage')->name('blog.remove.image');
                Route::get('remove-image-thumb', 'BlogController@removeThumbnail')->name('blog.remove.thumb');
            });
            //Sliders
            Route::namespace('Slider')->group(function () {
                Route::resource('sliders', 'SliderController');
                Route::get('remove-image-product', 'SliderController@removeImage')->name('slider.remove.image');
                Route::get('remove-image-thumb', 'SliderController@removeThumbnail')->name('slider.remove.thumb');
            });
            //CMS
            Route::namespace('Cms')->group(function () {
                Route::resource('cms', 'CmsController');
                
            });
            //Company Detail;
            Route::namespace('CompanyDetail')->group(function () {
                Route::resource('company_detail', 'CompanyDetailController');
                
            });
            //Testimonials
            Route::namespace('Testimonial')->group(function () {
                Route::resource('testimonials', 'TestimonialController');
                Route::get('remove-image-product', 'TestimonialController@removeImage')->name('testimonial.remove.image');
                Route::get('remove-image-thumb', 'TestimonialController@removeThumbnail')->name('testimonial.remove.thumb');
            });

            Route::namespace('Customers')->group(function () {
                Route::resource('customers', 'CustomerController');
                Route::resource('customers.addresses', 'CustomerAddressController');
            });
            Route::namespace('Categories')->group(function () {
                Route::resource('categories', 'CategoryController');
                Route::get('remove-image-category', 'CategoryController@removeImage')->name('category.remove.image');
            });
            Route::namespace('Orders')->group(function () {
                Route::resource('orders', 'OrderController');
                Route::resource('order-statuses', 'OrderStatusController');
                Route::get('orders/{id}/invoice', 'OrderController@generateInvoice')->name('orders.invoice.generate');
            });
            Route::resource('addresses', 'Addresses\AddressController');
            Route::resource('countries', 'Countries\CountryController');
            Route::resource('countries.provinces', 'Provinces\ProvinceController');
            Route::resource('countries.provinces.cities', 'Cities\CityController');
            Route::resource('couriers', 'Couriers\CourierController');
            Route::resource('attributes', 'Attributes\AttributeController');
            Route::resource('attributes.values', 'Attributes\AttributeValueController');
            Route::resource('brands', 'Brands\BrandController');

        });
        Route::group(['middleware' => ['role:admin|superadmin, guard:employee']], function () {
            Route::resource('employees', 'EmployeeController');
            Route::get('employees/{id}/profile', 'EmployeeController@getProfile')->name('employee.profile');
            Route::put('employees/{id}/profile', 'EmployeeController@updateProfile')->name('employee.profile.update');
            Route::resource('roles', 'Roles\RoleController');
            Route::resource('permissions', 'Permissions\PermissionController');
        });
    });
});

/**
 * Frontend routes
 */
Auth::routes();
Route::namespace('Auth')->group(function () {
    Route::get('cart/login', 'CartLoginController@showLoginForm')->name('cart.login');
    Route::post('cart/login', 'CartLoginController@login')->name('cart.login');
    Route::get('logout', 'LoginController@logout');
    Route::post('register', 'RegisterController@register');
});

Route::namespace('Front')->group(function () {
    Route::get('/old', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@old')->name('home');
    Route::group(['middleware' => ['auth', 'web']], function () {

        Route::namespace('Payments')->group(function () {
            Route::get('bank-transfer', 'BankTransferController@index')->name('bank-transfer.index');
            Route::post('bank-transfer', 'BankTransferController@store')->name('bank-transfer.store');
        });

        Route::namespace('Addresses')->group(function () {
            Route::resource('country.state', 'CountryStateController');
            Route::resource('state.city', 'StateCityController');
        });


        Route::get('accounts', 'AccountsController@index')->name('accounts');
        Route::get('checkout', 'CheckoutController@index')->name('checkout.index');
        Route::post('checkout', 'CheckoutController@store')->name('checkout.store');
        Route::get('checkout/execute', 'CheckoutController@executePayPalPayment')->name('checkout.execute');
        Route::post('checkout/execute', 'CheckoutController@charge')->name('checkout.execute');
        Route::get('checkout/cancel', 'CheckoutController@cancel')->name('checkout.cancel');
        Route::get('checkout/success', 'CheckoutController@success')->name('checkout.success');
        Route::resource('customer.address', 'CustomerAddressController');
    });
    // Route::resource('cart', 'CartController');
        Route::get('wishlist','HomeController@getWishlist');
        Route::get('blog/{slug}','HomeController@getBlogDetail');
        Route::get('blogs','HomeController@getBlogs');
        Route::get('blogs/{tag}','HomeController@getTaggedBlogs');
        Route::post('blog-review/submitreview','HomeController@submitReview');
        Route::get('minicart','AjaxController@getMinicart');
        Route::post('add-to-wishlist','AjaxController@addToWishlist');
        Route::post('delete-wishlist-item','AjaxController@deleteWishlistItem');
        Route::post('remove-product','AjaxController@deleteSessionData');
        Route::post('update-cart','AjaxController@updateCart');
        Route::post('cart/delete-product/{id}','AjaxController@deleteSessionData');
    Route::post('postCheckout', 'CheckoutController@postCheckout');

Route::post('submit-product-review', 'ProductController@submitReview');
Route::post('submit-newsletter', 'AjaxController@submitNewsletter');
Route::post('contact-form-submit', 'AjaxController@submitContactForm');


Route::post('pay', 'PayController@pay');


Route::get('payment-success', 'PayController@success');

        Route::get('contact_us','CmsController@getContact');
        Route::get('about_us','CmsController@getAbout');
        Route::get('terms_n_conditions','CmsController@getTNC');
        Route::get('privacy_policy','CmsController@getPrivacyPolicy');
        Route::get('return_policy','CmsController@getReturnPolicy');

    
    Route::get('cart', 'CartController@getCart');
    Route::post('add-to-cart', 'AjaxController@addTocart');
    Route::get("category/{slug}", 'CategoryController@getCategory')->name('front.category.slug');
    Route::get("searchedProducts", 'ProductController@search')->name('search.product');
    Route::get("product-detail/{product}", 'ProductController@show')->name('front.get.product');
    Route::get("/products/{product}", 'ProductController@showone')->name('front.get.product');
});