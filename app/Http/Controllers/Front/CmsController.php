<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Cms\Cms;

class CmsController extends Controller
{
    
    public function getContact()
    {
        return view('front.contact_us');
    }

   public function getAbout()
    {
         $content = Cms::where('page','About')->first();
        return view('front.about_us',compact('content'));
    }

   public function getTNC()
    {
        $content = Cms::where('page','Terms')->first();
        return view('front.policies',compact('content'));
    }

   public function getPrivacyPolicy()
    {
        $content = Cms::where('page','Privacy')->first();
        return view('front.policies',compact('content'));
    }

   public function getReturnPolicy()
    {
        $content = Cms::where('page','Return')->first();
        return view('front.policies',compact('content'));
    }

   
}
