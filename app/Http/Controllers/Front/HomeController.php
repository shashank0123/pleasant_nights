<?php

namespace App\Http\Controllers\Front;


use Illuminate\Http\Request;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Wishlist\Wishlist;
use App\Shop\Blog\Blog;
use App\Shop\Slider\Slider;
use App\Shop\Products\Product;
use App\Shop\Categories\Category;
use App\Shop\Testimonial\Testimonial;
use App\Shop\Brands\Brand;
use DB;

class HomeController
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * HomeController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepo = $categoryRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function old()
    {
        $brandProducts = array();
        $categoryProducts = array();
        $brands = array();
        // $cat1 = $this->categoryRepo->findCategoryById(2);
        // $cat2 = $this->categoryRepo->findCategoryById(3);
        $sliders = Slider::where('status','1')
        ->orderBy('created_at','DESC')
        ->limit(5)
        ->get();

        $blogs = Blog::where('status','1')
        ->orderBy('created_at','DESC')
        ->limit(3)
        ->get();

        $testimonials = Testimonial::where('status','1')
        ->orderBy('created_at','DESC')
        ->limit(4)
        ->get();

        $brand = Brand::orderBy('created_at','DESC')->get();

        if(!empty($brand)){
            $i=1;
            foreach($brand as $brnd){
                if($i<=4){
                    $products = Product::where('brand_id',$brnd->id)
                    ->where('status','1')
                    ->orderBy('created_at','DESC')
                    ->limit(4)
                    ->get();

                    if(!empty($products)){
                        $brands[] = $brnd;
                        $brandProducts[$brnd->id]['brand'] = $brnd;
                        $brandProducts[$brnd->id]['products'] = $products;
                        $i++;
                    }
                }
                else{
                    break;
                }
            }
        }


        $topCategories = Category::where('parent_id',NULL)
        ->where('status','1')
        ->orderBy('id','DESC')
        ->limit(4)
        ->get();


        if(!empty($topCategories)){
 
            foreach($topCategories as $category){
                $i=0;
                $cat = array();
                $cat[$i] = $category->id;
                $item = array();
                $cat2 = Category::where('parent_id',$category->id)->where('status',1)->get();
                if(!empty($cat2)){
                    foreach($cat2 as $cate2){
                        $cat[] = $cate2->id;
                        $cat3 = Category::where('parent_id',$cate2->id)->where('status',1)->get();
                        if(!empty($cat3)){
                            foreach($cat3 as $cate3){
                                $cat[] = $cate3->id;

                            }
                        }
                    }
                }
                $product = Product::join('category_product','category_product.product_id','products.id')
                ->where('products.status','1')->whereIn('category_product.category_id',$cat)->orderBy('created_at','DESC')->select('products.*')->limit(8)->get();

                if(count($product)>0){
                    $item['products']= $product;
                    $item['category']= $category;
                    $categoryProducts[] = $item;
                    $i++;
                }
            }
        }



        $new_arrivals = Product::join('brands','brands.id','products.brand_id')
        ->where('products.status','1')
        ->orderBy('products.created_at','DESC')
        ->select('products.*','brands.name as brand_name')
        ->limit(9)
        ->get();

        $latest_products = Product::where('status','1')
        ->orderBy('created_at','DESC')
        ->limit(3)
        ->get();

$new_products = Product::where('status','1')
        ->orderBy('created_at','DESC')
        ->limit(5)
        ->get();

        // echo count($sliders); die;

        return view('front.index',compact('sliders','new_arrivals','brands','brandProducts','latest_products','blogs','testimonials','topCategories','categoryProducts','new_products'));
        // return view('front.index2', compact('cat1', 'cat2'));
    }

    public function index()
    {
        $cat1 = $this->categoryRepo->findCategoryById(2);
        $cat2 = $this->categoryRepo->findCategoryById(3);

        // return view('front.index2');
        return view('front.index2', compact('cat1', 'cat2'));
    }

    public function getBlogs(){

        $recent_articles = Blog::orderBy('created_at','DESC')->where('status','1')->limit('3')->get();
        $blogs = Blog::orderBy('created_at','DESC')->where('status','1')->paginate(10);

$comments = array();
if(count($blogs)>0){

foreach($blogs as $blog){
    $comments[] = DB::table('blog_reviews')->where('blog_id',$blog->id)->count();
}
}


        $bestDeals = Product::where('status','1')->orderBy('created_at','DESC')->limit(5)->get();
        return view('front.blogs',compact('recent_articles','blogs','bestDeals','comments'));
    }

    public function getTaggedBlogs($tag){

        $recent_articles = Blog::orderBy('created_at','DESC')->where('status','1')->limit('3')->get();
        $blogs = Blog::orderBy('created_at','DESC')->where('status','1')->where('tags','LIKE','%'.$tag.'%')->paginate(10);

        $comments = array();
if(count($blogs)>0){

foreach($blogs as $blog){
    $comments[] = DB::table('blog_reviews')->where('blog_id',$blog->id)->count();
}
}

        $bestDeals = Product::where('status','1')->where('name','LIKE','%'.$tag.'%')->orderBy('created_at','DESC')->limit(5)->get();
        return view('front.blogs',compact('recent_articles','blogs','tag','bestDeals','comments'));
    }

    public function getBlogDetail($slug){

        $blog = Blog::where('slug',$slug)->first();
        $tags = array();
        if(!empty($blog->tags))
        $tags = explode(',',$blog->tags);


        $comments = DB::table('blog_reviews')->where('blog_id',$blog->id)->count();

        $reviews = DB::table('blog_reviews')->where('blog_id',$blog->id)->get();
        return view('front.blog-detail',compact('blog','comments','reviews','tags'));
    }

    public function submitReview(Request $request){
        $check = DB::table('blog_reviews')->where('email',$request->email)->first();
            $message = 'Something went wrong';

        if(!empty($check)){
            $message = 'A review is already posted from this email.';
        }
        else{


        $newData = DB::table('blog_reviews')->insert([
            'name' => $request->name ?? '',
            'email' => $request->email ?? '',
            'blog_review' => $request->blog_review ?? '',
            'blog_id' => $request->blog_id ?? '',
        ]);

        $message = 'Your review submitted successfully. Thank you.';
        }
        return redirect()->back()->with('successReview',$message);
            }
            

            public function getWishlist(){
                if(!auth()->check()){
                    redirect()->back()->with('wishlistLogin','Please First Login');
                }
                    $products = Wishlist::join('products','products.id','wishlists.product_id')->where('wishlists.user_id',auth()->user()->id)->select('products.*')->get();

                    return view('front.wishlist',compact('products'));
            }

}
