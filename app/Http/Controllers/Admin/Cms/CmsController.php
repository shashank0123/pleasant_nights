<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Http\Controllers\Controller;
use App\Shop\Cms\Repositories\CmsRepository;
use App\Shop\Cms\Repositories\CmsRepositoryInterface;
use App\Shop\Cms\Requests\CreateCmsRequest;
use App\Shop\Cms\Requests\UpdateCmsRequest;

class CmsController extends Controller
{
    /**
     * @var CmsRepositoryInterface
     */
    private $cmsRepo;

    /**
     * CmsController constructor.
     *
     * @param CmsRepositoryInterface $CmsRepository
     */
    public function __construct(CmsRepositoryInterface $cmsRepository)
    {
        $this->cmsRepo = $cmsRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = $this->cmsRepo->paginateArrayResults($this->cmsRepo->listCms(['*'], 'id', 'asc')->all());

        return view('admin.cms.list', ['cms' => $data]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.cms.create');
    }

    /**
     * @param CreateCmsRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateCmsRequest $request)
    {
        $this->cmsRepo->createCms($request->all());

        return redirect()->route('admin.cms.index')->with('message', 'Create cms successful!');
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        return view('admin.cms.edit', ['cms' => $this->cmsRepo->findCmsById($id)]);
    }

    /**
     * @param UpdateCmsRequest $request
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Shop\cms\Exceptions\UpdateCmsErrorException
     */
    public function update(UpdateCmsRequest $request, $id)
    {
        $cms = $this->cmsRepo->findCmsById($id);

        $cmsRepo = new CmsRepository($cms);
        $cmsRepo->updateCms($request->all());

        return redirect()->route('admin.cms.edit', $id)->with('message', 'Update successful!');
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $cms = $this->cmsRepo->findCmsById($id);

        $cmsRepo = new CmsRepository($cms);
        $cmsRepo->deleteCms();

        return redirect()->route('admin.cms.index')->with('message', 'Delete successful!');
    }
}
