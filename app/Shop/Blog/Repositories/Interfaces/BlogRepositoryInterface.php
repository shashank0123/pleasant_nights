<?php

namespace App\Shop\Blog\Repositories\Interfaces;

use App\Shop\AttributeValues\AttributeValue;
use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\Blog\Blog;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;


interface BlogRepositoryInterface extends BaseRepositoryInterface
{
    public function listBlogs(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection;

    public function createBlog(array $data) : Blog;

    public function updateBlog(array $data) : bool;

    public function findBlogById(int $id) : Blog;

    public function deleteBlog(Blog $blog) : bool;

    public function removeBlog() : bool;


    public function deleteFile(array $file, $disk = null) : bool;

    // public function deleteThumb(string $src) : bool;

    public function findBlogBySlug(array $slug) : Blog;

    public function searchBlog(string $text) : Collection;

    public function saveCoverImage(UploadedFile $file) : string;
}
