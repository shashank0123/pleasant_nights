<?php

namespace App\Shop\Blog\Repositories;

use App\Shop\AttributeValues\AttributeValue;
use App\Shop\Blog\Exceptions\BlogCreateErrorException;
use App\Shop\Blog\Exceptions\BlogUpdateErrorException;
use App\Shop\Tools\UploadableTrait;
use Jsdecena\Baserepo\BaseRepository;
use App\Shop\Blog\Exceptions\BlogNotFoundException;
use App\Shop\Blog\Blog;
use App\Shop\Blog\Repositories\Interfaces\BlogRepositoryInterface;
use App\Shop\Blog\Transformations\BlogTransformable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class BlogRepository extends BaseRepository implements BlogRepositoryInterface
{
    use BlogTransformable, UploadableTrait;

    /**
     * ProductRepository constructor.
     * @param Product $product
     */
    public function __construct(Blog $blog)
    {
        parent::__construct($blog);
        $this->model = $blog;
    }

    /**
     * List all the products
     *
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return Collection
     */
    public function listBlogs(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * Create the product
     *
     * @param array $data
     *
     * @return Product
     * @throws BlogCreateErrorException
     */
    public function createBlog(array $data) : Blog
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new BlogCreateErrorException($e);
        }
    }

    /**
     * Update the product
     *
     * @param array $data
     *
     * @return bool
     * @throws BlogUpdateErrorException
     */
    public function updateBlog(array $data) : bool
    {
        $filtered = collect($data)->except(['_token','_method'])->all();

        try {
        // var_dump($data); die;
            return $this->model->where('id', $this->model->id)->update($filtered);
        } catch (QueryException $e) {
            throw new BlogUpdateErrorException($e);
        }
    }

    /**
     * Find the product by ID
     *
     * @param int $id
     *
     * @return Blog
     * @throws BlogNotFoundException
     */
    public function findBlogById(int $id) : Blog
    {
        try {
            return $this->transformBlog($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new BlogNotFoundException($e);
        }
    }

    /**
     * Delete the Blog
     *
     * @param Blog $Blog
     *
     * @return bool
     * @throws \Exception
     * @deprecated
     * @use removeBlog
     */
    public function deleteBlog(Blog $Blog) : bool
    {
        // $Blog->images()->delete();
        return $Blog->delete();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function removeBlog() : bool
    {
        return $this->model->where('id', $this->model->id)->delete();
    }

    /**
     * Detach the categories
     */
   
    /**
     * Return the categories which the product is associated with
     *
     * @return Collection
     */
    
    /**
     * Sync the categories
     *
     * @param array $params
     */
   

    /**
     * @param $file
     * @param null $disk
     * @return bool
     */
    public function deleteFile(array $file, $disk = null) : bool
    {
        return $this->update(['cover' => null], $file['blog']);
    }

    /**
     * @param string $src
     * @return bool
     */
    

    /**
     * Get the product via slug
     *
     * @param array $slug
     *
     * @return Product
     * @throws BlogNotFoundException
     */
    public function findBlogBySlug(array $slug) : Blog
    {
        try {
            return $this->findOneByOrFail($slug);
        } catch (ModelNotFoundException $e) {
            throw new BlogNotFoundException($e);
        }
    }

    /**
     * @param string $text
     * @return mixed
     */
    public function searchBlog(string $text) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchBlog($text);
        } else {
            return $this->listBlogs();
        }
    }

    /**
     * @return mixed
     */
   

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function saveCoverImage(UploadedFile $file) : string
    {
        return $file->store('blogs', ['disk' => 'public']);
    }

}
