<?php

namespace App\Shop\Inventory\Requests;

use App\Shop\Base\BaseFormRequest;

class UpdateInventoryRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
