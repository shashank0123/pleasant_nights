<?php

namespace App\Shop\Inventory\Exceptions;

class InventoryNotFoundErrorException extends \Exception
{
}
