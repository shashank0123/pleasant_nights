<?php

namespace App\Shop\Testimonial\Repositories\Interfaces;

use App\Shop\AttributeValues\AttributeValue;
use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\Testimonial\Testimonial;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;


interface TestimonialRepositoryInterface extends BaseRepositoryInterface
{
    public function listTestimonials(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection;

    public function createTestimonial(array $data) : Testimonial;

    public function updateTestimonial(array $data) : bool;

    public function findTestimonialById(int $id) : Testimonial;

    public function deleteTestimonial(Testimonial $testimonial) : bool;

    public function removeTestimonial() : bool;


    public function deleteFile(array $file, $disk = null) : bool;

    // public function deleteThumb(string $src) : bool;

    public function findTestimonialBySlug(array $slug) : Testimonial;

    public function searchTestimonial(string $text) : Collection;

    public function saveCoverImage(UploadedFile $file) : string;
}
