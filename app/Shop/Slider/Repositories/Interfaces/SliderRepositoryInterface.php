<?php

namespace App\Shop\Slider\Repositories\Interfaces;

use App\Shop\AttributeValues\AttributeValue;
use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\Slider\Slider;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;


interface SliderRepositoryInterface extends BaseRepositoryInterface
{
    public function listSliders(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection;

    public function createSlider(array $data) : Slider;

    public function updateSlider(array $data) : bool;

    public function findSliderById(int $id) : Slider;

    public function deleteSlider(Slider $slider) : bool;

    public function removeSlider() : bool;


    public function deleteFile(array $file, $disk = null) : bool;

    // public function deleteThumb(string $src) : bool;

    public function findSliderBySlug(array $slug) : Slider;

    public function searchSlider(string $text) : Collection;

    public function saveCoverImage(UploadedFile $file) : string;
}
