<?php

namespace App\Shop\Cms\Repositories;

use Jsdecena\Baserepo\BaseRepository;
use App\Shop\Cms\Cms;
use App\Shop\Cms\Exceptions\CmsNotFoundErrorException;
use App\Shop\Cms\Exceptions\CreateCmsErrorException;
use App\Shop\Cms\Exceptions\UpdateCmsErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

class CmsRepository extends BaseRepository implements CmsRepositoryInterface
{
    /**
     * CmsRepository constructor.
     *
     * @param cms $cms
     */
    public function __construct(Cms $cms)
    {
        parent::__construct($cms);
        $this->model = $cms;
    }

    /**
     * @param array $data
     *
     * @return cms
     * @throws CreateCmsErrorException
     */
    public function createCms(array $data) : Cms
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new CreateCmsErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return cms
     * @throws CmsNotFoundErrorException
     */
    public function findCmsById(int $id) : Cms
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new CmsNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdateCmsErrorException
     */
    public function updateCms(array $data) : bool
    {
        try {
            return $this->update($data);
        } catch (QueryException $e) {
            throw new UpdateCmsErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteCms() : bool
    {
        return $this->delete();
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listCms($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    


}
