<?php

namespace App\Shop\Cms\Repositories;

use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\Cms\Cms;
use Illuminate\Support\Collection;

interface CmsRepositoryInterface extends BaseRepositoryInterface
{
    public function createCms(array $data): Cms;

    public function findCmsById(int $id) : Cms;

    public function updateCms(array $data) : bool;

    public function deleteCms() : bool;

    public function listCms($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

}
