 <script src="{{asset('images/t/4/assets/option_selection-fe6b72c2bbdd3369ac0bfefe8648e3c889efca213baefd4cfb0dd9363563831f.js')}}" type="text/javascript"></script>
 <script src="{{asset('images/t/4/assets/jquery.fancybox.js?v=4103147835157344137')}}" type="text/javascript"></script>

 
 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-525fbbd6215b4f1a"></script>


 <script>
   function sprUpdateCount(e){
   }


function showSizewisePrice(id,price,size){

  $('#ProductPrice').text('RS. '+price);
  $('#ProductPrice1').text('RS. '+price);
  $('#productSize'+id).val(size);
  $('#productPrice'+id).val(price);
}

function getMethod(pm){
  $('#payment_method').val(pm);
}

function getBillAdd(pm){
  $('#billing_add').val(pm);
}

function getDelAdd(pm){
  $('#delivery_add').val(pm);
}

 </script>

@if(!empty(session()->get('successReview')))
<script type="text/javascript">
  Swal(<?php echo "'".session()->get('successReview')."'";?>)
</script>
@endif


@if(!empty(session()->get('wishlistLogin')))
<script type="text/javascript">
  Swal(<?php echo "'".session()->get('wishlistLogin')."'";?>)
</script>
@endif

<script type="text/javascript">
  function confirmDelete(id){   
    swal({
  title: "Do you want to remove this product ?",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
      deleteWishlistItem(id);
    
  } 
});
    
  }
</script>





<script type="text/javascript">
  function plusProductQty(id){
    var quantityPd = $('#quantityPd'+id).val();


    var plusquantityId = parseInt(quantityPd) + 1;

    $('#quantityPd'+id).val(plusquantityId);

    $('.min-qty-alert').hide();
  }

   function minusProductQty(id){
    var quantityPd = $('#quantityPd'+id).val();

    if(parseInt(quantityPd)>1){
    var minusquantityId = parseInt(quantityPd) - 1;

    $('#quantityPd'+id).val(minusquantityId);
  }
  else{
    $('.min-qty-alert').show();
  }
  }
</script>



<script type="text/javascript">
  function plus(id){
    var productQuantity = $('#productQuantity'+id).val();


    var plusquantityId = parseInt(productQuantity) + 1;

    $('#productQuantity'+id).val(plusquantityId);

    updateCart(id,'plus');

  }

   function minus(id){
    var productQuantity = $('#productQuantity'+id).val();

    if(parseInt(productQuantity)>1){
    var minusquantityId = parseInt(productQuantity) - 1;

    $('#productQuantity'+id).val(minusquantityId);
    updateCart(id,'minus');
  }
  
  }



  function updateCart(id,action){
     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  

    $.ajax({
     url: '/update-cart',
     type: 'POST',
     data: {_token: CSRF_TOKEN, id: id, action:action},
     success: function (data) {
       $("#total"+id).empty();
       $("#total"+id).html("RS. " + data.total + ".00");
     
       $("#totalBill").empty();
       $("#totalBill").html("RS. " + data.bill + ".00");

     },
     failure: function (data) {
       alert('Something went wrong');
     }
   });
  }
</script>






 <script type="text/javascript">
  function showTab(i){
    if(i==0){
      $('#tabContent0').show();
      $('#tabContent1').hide();
      $('#tabContent2').hide();
      $('#tabContent3').hide();
    }
    if(i==1){
      $('#tabContent0').hide();
      $('#tabContent1').show();
      $('#tabContent2').hide();
      $('#tabContent3').hide();
    }
    if(i==2){
      $('#tabContent0').hide();
      $('#tabContent1').hide();
      $('#tabContent2').show();
      $('#tabContent3').hide();
    }
    if(i==3){
      $('#tabContent0').hide();
      $('#tabContent1').hide();
      $('#tabContent2').hide();
      $('#tabContent3').show();
    }
  }
</script>

<script>

  function getMiniCart(){
    jQuery.ajax({
     url: '/minicart',
     type: 'GET',
     success: function (data) {

      $('#slidedown-cart').html(data);

    },
    failure: function (data) {
     Swal(data.message);
   }
 });
  }

  function showGridView(){

    $('#gridView').show();
    $('#grid').addClass('active');
    $('#list').removeClass('active');

    $('#listView').hide();

  }

  function showListView(){
    $('#listView').show();
    $('#list').addClass('active');
    $('#grid').removeClass('active');

    $('#gridView').hide();

  }


function buyNow(id){
  addToCart(id);

  var user_id = <?php echo auth()->user()->id ?? 0 ?>;

  if(user_id>0){

    window.location.href = '/checkout'
  }
  else{
    window.location.href = '/login'
  }
}


// Add to Cart
function addToCart(id){
  var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
        // Swal(id);
        var quantity = jQuery('#quantityPd'+id).val();
        var stock = jQuery('#stock'+id).val();
        var size = jQuery('#productSize'+id).val();
        var price = jQuery('#productPrice'+id).val();


        if(stock == 'yes'){
          jQuery.ajax({
           url: '/add-to-cart',
           type: 'POST',
           data: {_token: CSRF_TOKEN, id: id, quantity: quantity, product_size : size, product_price : price},
           success: function (data) {
           // Swal(data.cartItem);
           jQuery('#cartCount').html(data.cartItem);

           jQuery('#cartSuccess').text('Product added to cart successfully');
           jQuery("#cartSuccess").hide().slideDown();
           setTimeout(function(){
            $("#cartSuccess").hide();        
          }, 3000);
         },
         failure: function (data) {
           Swal(data.message);
         }
       });
        }
        else{
          Swal('Product Out Of Stock.');
        }
      }




// Add to Wishlist
function addToWishlist(id){
  var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
  userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

  if(userId == 0){
    Swal('Please Login First');
  }
  else{
        // Swal(id);

        jQuery.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           Swal(data.message);
           

           
           setTimeout(function(){
           window.reload();       
          }, 3000);

         },
         failure: function (data) {
           Swal(data);
         }
       });
      }
    }


    function deleteWishlistItem(id){
      var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
      user_id = @if(!empty(auth()->user())){{auth()->user()->id}}@else{{'0'}}@endif;
          // alert(id+" / "+user_id);

          jQuery.ajax({
           url: '/delete-wishlist-item',
           type: 'POST',
           data: {_token: CSRF_TOKEN, id: id, user_id : user_id},
           success: function (data) {
               window.location.href = '/wishlist';
             },
             failure: function (data) {
               // alert(data.message);
             }
           });
        }

function removeProduct(id){

  var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
      
          jQuery.ajax({
           url: '/remove-product',
           type: 'POST',
           data: {_token: CSRF_TOKEN, id: id},
           success: function (data) {
               // alert(data.flag);
               jQuery('#cartCount').html(data.cartItem);
               jQuery('#totalCart').html(data.bill);
               
               jQuery('#div-list'+id).hide();
             },
             failure: function (data) {
               // alert(data.message);
             }
           });

}

        function deleteCartProduct(id){
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    // count--;

    $.ajax({
      /* the route pointing to the post function */
      url: '/cart/delete-product/'+id,
      type: 'POST',
      datatype: 'JSON',
      /* send the csrf-token and the input to the controller */
      data: {_token: CSRF_TOKEN, id: id},
      success: function (data) {
        $('#cartTr'+id).hide();
        
        $('#cartCount').html(data.cartItem);

       $("#totalBill").empty();
       $("#totalBill").html("RS. " + data.bill + ".00");

        if(data.bill <= 0){
          $("#checkoutLink").attr("href", "/");
          $("#checkoutLink").html('Continue Shopping');
        }
      }
    });
  }


  function decrement(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var action = 'minus' ;
    var quantity = $('#quantityPd'+id).val();
    if(quantity>1){

      $('#quantityPd'+id).val(--quantity);
      $.ajax({
       url: '/update-cart',
       type: 'POST',
       data: {_token: CSRF_TOKEN, id: id, action: action},
       success: function (data) {
        $("#subTotal"+id).empty();
        $("#subTotal"+id).html("₹" + data.total);
        $("#total_price").empty();
        $("#total_price").append("₹" + data.bill);
        $("#grand_total").html("₹" + data.bill);
        $("#amount"+id).html("₹" + data.total);


      },
      failure: function (data) {
       alert(data.message);
     }
   });
    }
  }

  function increment(id){

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var quantity = $('#quantityPd'+id).val();
    var action = 'plus' ;

    $('#quantityPd'+id).val(++quantity);
    $.ajax({
     url: '/update-cart',
     type: 'POST',
     data: {_token: CSRF_TOKEN, id: id, action:action},
     success: function (data) {
       $("#subTotal"+id).empty();
       $("#subTotal"+id).html("₹" + data.total);
       $("#total_price").empty();
       $("#total_price").append("₹" + data.bill);
       $("#grand_total").html("₹" + data.bill);
       $("#amount"+id).html("₹" + data.total);

     },
     failure: function (data) {
       alert('Something went wrong');
     }
   });
  }


</script>




<script>
  jQuery(".button").on("click", function() {
   var oldValue = jQuery("#quantity").val(),
   newVal = 1;

   if (jQuery(this).text() == "+") {
    newVal = parseInt(oldValue) + 1;
  } else if (oldValue > 1) {
    newVal = parseInt(oldValue) - 1;
  }

  jQuery(".product-single #quantity").val(newVal);

  updatePricing();

});




  function updatePricing() {}



</script>








<script type="text/javascript">
  
function checkStock(){
  
}
</script>


<script type="text/javascript">
 $(document).ready(function(){
   var related = $(".related-products");
   related.owlCarousel({
    loop:true,       
    nav:true,
    navContainer: ".nav_featured",
    navText: ['<a class="prev"><i class="btn fas fa-angle-left"></i></a>','<a class="next"><i class="btn fas fa-angle-right"></i></a>'],
    dots: false,
    responsive:{
     0:{
      items:2
    },
    968:{
      items:3
    },
    1000:{
      items: 4
    }
  }
});
 });

</script>








<script type="text/javascript">
  $(document).ready(function(){
   var productSidedeals = $(".sidebar-deal-products");
   productSidedeals.owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navContainer: ".product-sidebar-deals-nav",
    navText: [' <a class="prev btn active"><i class="fa fa-angle-left"></i></a>',' <a class="next btn active"><i class="fa fa-angle-right"></i></a>'],
    dots: false,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:1
      },
      1000:{
        items:1
      }
    }
  });      
 });
</script>



<script type="text/javascript">
  $(document).ready(function(){
   $("#promo-carousel").owlCarousel({ 
    loop:false,
    
    nav:false,       
    dots: true,
    responsive:{
     0:{
       items:1
     },
     600:{
       items:1
     },
     1000:{
       items:1
     }
   }

 });
 });

</script>

<script >
 function showReviewForm(){
  var id = <?php echo auth()->user()->id ?? '0'?>;

  if(id>0){
  $('.spr-form').toggle();
}
else{
Swal("You can't submit your review. First purchase the product. ");
}

}
</script>