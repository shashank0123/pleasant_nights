<style>
    
     .img-mattress{
            width: auto;
            height: 600px;
        }
    
    .point1{
            position: absolute;
    /* top: 25px; */
    margin-top: -47%;
    left: 35%;
    height: 30px;
    width: 30px;
    /*background-color: #69e0ff;*/
    background-color: #003A5D;
    color: #fff;
    border-radius: 50%;
    text-align: center;
    padding-top: 2px;
    cursor: pointer;
    }
    
     .point2{
            position: absolute;
    /* top: 25px; */
    margin-top: -43%;
    left: 36%;
    height: 30px;
    width: 30px;
    /*background-color: #69e0ff; */
    background-color: #003A5D;
    color: #fff;
    border-radius: 50%;
    text-align: center;
    padding-top: 2px;
    cursor: pointer;
    }
    
     .point3{
            position: absolute;
    /* top: 25px; */
    margin-top: -39%;
    left: 36%;
    height: 30px;
    width: 30px;
    /*background-color: #69e0ff;*/
    background-color: #003A5D;
    color: #fff;
    border-radius: 50%;
    text-align: center;
    padding-top: 2px;
    cursor: pointer;
    }
    
     .point4{
            position: absolute;
    /* top: 25px; */
    margin-top: -35.5%;
    left: 35%;
    height: 30px;
    width: 30px;
    /*background-color: #69e0ff;*/
    background-color: #003A5D;
    color: #fff;
    border-radius: 50%;
    text-align: center;
    padding-top: 2px;
    cursor: pointer;
    }
    
     .point5{
            position: absolute;
    /* top: 25px; */
    margin-top: -31.8%;
    left: 34%;
    height: 30px;
    width: 30px;
    /*background-color: #69e0ff;*/
    background-color: #003A5D;
    color: #fff;
    border-radius: 50%;
    text-align: center;
    padding-top: 2px;
    cursor: pointer;
    }
    
     .point6{
            position: absolute;
    /* top: 25px; */
    margin-top: -28%;
    left: 34%;
    height: 30px;
    width: 30px;
    /*background-color: #69e0ff;*/
    background-color: #003A5D;
    color: #fff;
    border-radius: 50%;
    text-align: center;
    padding-top: 2px;
    cursor: pointer;
    cursor: pointer;
    }
    
    @media screen and (max-width: 1300px){
     .point1{
    margin-top: -49%;
    left: 37%;
    }
    
     .point2{
    margin-top: -45%;
    left: 36.75%;
    }
    
     .point3{
    margin-top: -41.25%;
    left: 37%;
    }
    
     .point4{
    margin-top: -38%;
    left: 36%;
    }
    
     .point5{
    margin-top: -33.5%;
    left: 37%;
    }
    
     .point6{
    margin-top: -30%;
    left: 36%;
    }
    }
    
    
    
    @media screen and (max-width: 1250px){
     .point1{
    margin-top: -49.75%;
    left: 39%;
    }
    
     .point2{
    margin-top: -45.75%;
    left: 38.5%;
    }
    
     .point3{
    margin-top: -42.25%;
    left: 39%;
    }
    
     .point4{
    margin-top: -38.50%;
    left: 38%;
    }
    
     .point5{
    margin-top: -34.5%;
    left: 38.5%;
    }
    
     .point6{
    margin-top: -30.5%;
    left: 37.5%;
    }
    }
    
    
    @media screen and (max-width: 1225px){
     .point1{
    margin-top: -50.75%;
    left: 39.5%;
    }
    
     .point2{
    margin-top: -46.75%;
    left: 39%;
    }
    
     .point3{
    margin-top: -42.75%;
    left: 39%;
    }
    
     .point4{
    margin-top: -38.50%;
    left: 38%;
    }
    
     .point5{
    margin-top: -34.5%;
    left: 38.5%;
    }
    
     .point6{
    margin-top: -30.5%;
    left: 37.5%;
    }
    }
    
    
    @media screen and (max-width: 1200px){
        .point1{
    margin-top: -51.75%;
    left: 40.5%;
    }
    
     .point2{
    margin-top: -47.75%;
    left: 40%;
    }
    
     .point3{
    margin-top: -43.25%;
    left: 40%;
    }
    
     .point4{
    margin-top: -39.25%;
    left: 39.5%;
    }
    
     .point5{
    margin-top: -35.25%;
    left: 39%;
    }
    
     .point6{
    margin-top: -31%;
    left: 39%;
    }
    }
    
    
    @media screen and (max-width: 1175px){
        .point1{
    margin-top: -53%;
    left: 41.5%;
    }
    
     .point2{
    margin-top: -48.25%;
    left: 40.75%;
    }
    
     .point3{
    margin-top: -44%;
    left: 40.75%;
    }
    
     .point4{
    margin-top: -39.75%;
    left: 40.5%;
    }
    
     .point5{
    margin-top: -35.60%;
    left: 40.2%;
    }
    
     .point6{
    margin-top: -31.75%;
    left: 40%;
    }
    }
    
    @media screen and (max-width: 768px){
        .img-mattress{
            width: 100%;
            height: 440px;
        }
        
         .point1{
    margin-top: -84%;
    left: 66%;
    }
    
     .point2{
    margin-top: -74%;
    left: 53%;
    }
    
     .point3{
    margin-top: -62%;
    left: 69%;
    }
    
     .point4{
    margin-top: -61%;
    left: 54%;
    }
    
     .point5{
    margin-top: -50%;
    left: 70%;
    }
    
     .point6{
    margin-top: -46%;
    left: 54%;
    }
        
        
    }
    
    @media screen and (max-width: 640px){
        .img-mattress{
            width: 100%;
            height: 380px;
        }
    }
    
    @media screen and (max-width: 550px){
        .img-mattress{
            width: 100%;
            height: 300px;
        }
    }
    
     @media screen and (max-width: 480px){
        .img-mattress{
            width: 100%;
            height: auto;
        }
        
            .point1{
    margin-top: -84%;
    left: 66%;
    }
    
     .point2{
    margin-top: -74%;
    left: 53%;
    }
    
     .point3{
    margin-top: -62%;
    left: 69%;
    }
    
     .point4{
    margin-top: -61%;
    left: 54%;
    }
    
     .point5{
    margin-top: -50%;
    left: 70%;
    }
    
     .point6{
    margin-top: -46%;
    left: 54%;
    }
        
    }
    
    
    @media screen and (max-width: 425px){
        
            .point1{
    margin-top: -82%;
    left: 63%;
    }
    
     .point2{
    margin-top: -72%;
    left: 58%;
    }
    
     .point3{
    margin-top: -63%;
    left: 67%;
    }
    
     .point4{
    margin-top: -61%;
    left: 54%;
    }
    
     .point5{
    margin-top: -48%;
    left: 70%;
    }
    
     .point6{
    margin-top: -48%;
    left: 55%;
    }
        
    }
    
    
   
   
   
   
   
    
    
    
    .ortho-response{
        position: relative;
    }
    
    
    
    
    
    
/*    [data-tooltip] {*/
/*  position: relative;*/
/*  z-index: 2;*/
/*  cursor: pointer;*/
/*}*/

/* Hide the tooltip content by default */
[data-tooltip]:before,
[data-tooltip]:after {
  visibility: hidden;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  pointer-events: none;
}

/* Position tooltip above the element */
[data-tooltip]:before {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-bottom: 5px;
  margin-left: -80px;
  padding: 7px;
  width: 200px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #000;
  background-color: hsla(0, 0%, 20%, 0.9);
  color: #fff;
  content: attr(data-tooltip);
  text-align: center;
  font-size: 14px;
  line-height: 1.2;
}

/* Triangle hack to make tooltip look like a speech bubble */
[data-tooltip]:after {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-left: -5px;
  width: 0;
  border-top: 5px solid #000;
  border-top: 5px solid hsla(0, 0%, 20%, 0.9);
  border-right: 5px solid transparent;
  border-left: 5px solid transparent;
  content: " ";
  font-size: 0;
  line-height: 0;
}

/* Show tooltip content on hover */
[data-tooltip]:hover:before,
[data-tooltip]:hover:after {
  visibility: visible;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
  opacity: 1;
}
    
    
    
    
    
    
    
</style>

    <div class="ortho-response">
    <img src="{{ asset('images/mattress/ortho_soft.png') }}" class="img-mattress">
    
    <div class="point1" data-tooltip="Fine Quality Knited Fabric 240 GSM">1</div>
    <div class="point2" data-tooltip="16 MM HD Quitting">2</div>
    <div class="point3" data-tooltip="Super Soft Layer 32D 30MM">3</div>
    <div class="point4" data-tooltip="Rebonded/Orthopedic Support Layer 85D 70MM">4</div>
    <div class="point5" data-tooltip="BMM HD Quitting">5</div>
    <div class="point6" data-tooltip="Fine Quality Knited Fabric 240 GSM">1</div>
    </div>
    
    